package cn.org.atool.fluent.mybatis.base.crud;

/**
 * 默认行为接口
 *
 * @author wudarui
 */
public interface IDefault extends IDefaultGetter, IDefaultSetter {
}
